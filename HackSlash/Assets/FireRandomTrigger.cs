﻿using System.Linq;
using UnityEngine;

/// <summary>
/// Fires random trigger from string[] triggers 
/// </summary>
public class FireRandomTrigger : StateMachineBehaviour
{
    /// <summary>
    /// Set this in inspector
    /// </summary>
    public string[] triggers;

    private int[] _triggers;

    /// <summary>
    /// Cache trigger names (string) to hashes (integer) for faster animation invoking
    /// </summary>
    public void Awake() => _triggers = triggers.Select(Animator.StringToHash).ToArray();

    /// <summary>
    /// Fire random trigger from _triggers 
    /// </summary>
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetTrigger(_triggers[Random.Range(0, _triggers.Length)]);
    }
    
}