﻿using Player;
using UnityEngine;

public class MobAttack : MonoBehaviour
{
    public float attackCooldown = 1f;
    public int damageToPlayer = 20;
    public Animator animator;
    public Rigidbody rgb;
    private bool isTouchingPlayer;
    private static readonly int Attack = Animator.StringToHash("Attack");
    private static PlayerHealth _playerHealth;

    private float nextAttackTime;
    private bool canAttack => Time.time >= nextAttackTime;

    private void Start()
    {
        rgb = GetComponent<Rigidbody>();
        rgb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationY;
        Invoke("coll", 1f);
        _playerHealth = GameObject.FindWithTag("Player").GetComponent<PlayerHealth>();
    }

    /// <summary>
    /// Turns collider on 1 second after creation
    /// </summary>
    void coll()
    {
        rgb.constraints = RigidbodyConstraints.None | RigidbodyConstraints.None;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.CompareTag("Player") || !canAttack) return;

        animator.SetTrigger(Attack);
        isTouchingPlayer = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            isTouchingPlayer = false;
    }

    /// <summary>
    /// Called in middle of attack animation
    /// </summary>
    public void HitEvent()
    {
        nextAttackTime = Time.time + attackCooldown;
        
        if (isTouchingPlayer)
            _playerHealth.TakeDamage(damageToPlayer);
    }
}