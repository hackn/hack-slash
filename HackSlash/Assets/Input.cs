// GENERATED AUTOMATICALLY FROM 'Assets/Input.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Input : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Input()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Input"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""e4540e5d-76ba-44bd-8127-b504a3b952de"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""01e47d9e-146f-4a31-9e15-817f0c9cbd1d"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""55f656c4-e92d-42df-bf32-6ffb0da5b2fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DropWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""d3354f40-3ec5-4d25-8580-39977140a607"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectNextWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""54df1565-ad92-4d31-98a7-4f608a885b8d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""9f4fbcbc-c554-4823-a5d0-209f79aba054"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""QuickUse1"",
                    ""type"": ""Button"",
                    ""id"": ""47041ca1-b0c0-4b3d-a3ee-86eb10be5b8b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""QuickUse2"",
                    ""type"": ""Button"",
                    ""id"": ""21694016-8fc6-4837-9f3d-3390c7122133"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""QuickUse3"",
                    ""type"": ""Button"",
                    ""id"": ""f19f98a8-7b78-4a15-a376-cce9a5b29b6d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""QuickUse4"",
                    ""type"": ""Button"",
                    ""id"": ""6095b3e3-09df-4e0e-9958-729e57b0c765"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""9ae678e3-82c0-443d-b9ab-3287faedb959"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Tap,SlowTap(pressPoint=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Dpad"",
                    ""id"": ""20b84e25-25d3-4903-a8f4-121475cb7ad8"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""667c48fa-f5cd-49ad-a330-3ca6ba4258f3"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""300e6d6d-0833-4ef0-9936-a8e4b4962af1"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""5fbffa5f-b834-459e-8024-9d7beda5e73e"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d6d348e0-7fbf-4f25-9532-4b74d6e6f5d5"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""929f6cd7-51ca-4f9e-84c5-7ae1fe2394fe"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1efb7d4a-975f-4382-8df4-7efbd0493f17"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DropWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""62889f92-07c3-452d-a901-35160dc102e0"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SelectNextWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0c1fd325-00f9-43e3-a68a-eec1c2c631d5"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""854a59e8-0526-4727-9b19-946066e7ec5b"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""QuickUse1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e70af35-0f4a-4b8a-a80e-931e9a2cd62f"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""QuickUse2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40b2a4e8-8f34-40f8-9817-efde75b908d4"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""QuickUse3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b0eb580e-63f3-46f6-9867-b617db242bd7"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""QuickUse4"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""708da21a-3514-404e-9e13-74eeaa3f49dc"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b6d38590-483d-48ec-8819-5e9a390c5bc2"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Move = m_Player.FindAction("Move", throwIfNotFound: true);
        m_Player_Dash = m_Player.FindAction("Dash", throwIfNotFound: true);
        m_Player_DropWeapon = m_Player.FindAction("DropWeapon", throwIfNotFound: true);
        m_Player_SelectNextWeapon = m_Player.FindAction("SelectNextWeapon", throwIfNotFound: true);
        m_Player_Cancel = m_Player.FindAction("Cancel", throwIfNotFound: true);
        m_Player_QuickUse1 = m_Player.FindAction("QuickUse1", throwIfNotFound: true);
        m_Player_QuickUse2 = m_Player.FindAction("QuickUse2", throwIfNotFound: true);
        m_Player_QuickUse3 = m_Player.FindAction("QuickUse3", throwIfNotFound: true);
        m_Player_QuickUse4 = m_Player.FindAction("QuickUse4", throwIfNotFound: true);
        m_Player_Attack = m_Player.FindAction("Attack", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Move;
    private readonly InputAction m_Player_Dash;
    private readonly InputAction m_Player_DropWeapon;
    private readonly InputAction m_Player_SelectNextWeapon;
    private readonly InputAction m_Player_Cancel;
    private readonly InputAction m_Player_QuickUse1;
    private readonly InputAction m_Player_QuickUse2;
    private readonly InputAction m_Player_QuickUse3;
    private readonly InputAction m_Player_QuickUse4;
    private readonly InputAction m_Player_Attack;
    public struct PlayerActions
    {
        private @Input m_Wrapper;
        public PlayerActions(@Input wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Player_Move;
        public InputAction @Dash => m_Wrapper.m_Player_Dash;
        public InputAction @DropWeapon => m_Wrapper.m_Player_DropWeapon;
        public InputAction @SelectNextWeapon => m_Wrapper.m_Player_SelectNextWeapon;
        public InputAction @Cancel => m_Wrapper.m_Player_Cancel;
        public InputAction @QuickUse1 => m_Wrapper.m_Player_QuickUse1;
        public InputAction @QuickUse2 => m_Wrapper.m_Player_QuickUse2;
        public InputAction @QuickUse3 => m_Wrapper.m_Player_QuickUse3;
        public InputAction @QuickUse4 => m_Wrapper.m_Player_QuickUse4;
        public InputAction @Attack => m_Wrapper.m_Player_Attack;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Dash.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @DropWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDropWeapon;
                @DropWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDropWeapon;
                @DropWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDropWeapon;
                @SelectNextWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSelectNextWeapon;
                @SelectNextWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSelectNextWeapon;
                @SelectNextWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSelectNextWeapon;
                @Cancel.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCancel;
                @QuickUse1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse1;
                @QuickUse1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse1;
                @QuickUse1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse1;
                @QuickUse2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse2;
                @QuickUse2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse2;
                @QuickUse2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse2;
                @QuickUse3.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse3;
                @QuickUse3.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse3;
                @QuickUse3.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse3;
                @QuickUse4.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse4;
                @QuickUse4.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse4;
                @QuickUse4.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnQuickUse4;
                @Attack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @DropWeapon.started += instance.OnDropWeapon;
                @DropWeapon.performed += instance.OnDropWeapon;
                @DropWeapon.canceled += instance.OnDropWeapon;
                @SelectNextWeapon.started += instance.OnSelectNextWeapon;
                @SelectNextWeapon.performed += instance.OnSelectNextWeapon;
                @SelectNextWeapon.canceled += instance.OnSelectNextWeapon;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @QuickUse1.started += instance.OnQuickUse1;
                @QuickUse1.performed += instance.OnQuickUse1;
                @QuickUse1.canceled += instance.OnQuickUse1;
                @QuickUse2.started += instance.OnQuickUse2;
                @QuickUse2.performed += instance.OnQuickUse2;
                @QuickUse2.canceled += instance.OnQuickUse2;
                @QuickUse3.started += instance.OnQuickUse3;
                @QuickUse3.performed += instance.OnQuickUse3;
                @QuickUse3.canceled += instance.OnQuickUse3;
                @QuickUse4.started += instance.OnQuickUse4;
                @QuickUse4.performed += instance.OnQuickUse4;
                @QuickUse4.canceled += instance.OnQuickUse4;
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    public interface IPlayerActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnDropWeapon(InputAction.CallbackContext context);
        void OnSelectNextWeapon(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
        void OnQuickUse1(InputAction.CallbackContext context);
        void OnQuickUse2(InputAction.CallbackContext context);
        void OnQuickUse3(InputAction.CallbackContext context);
        void OnQuickUse4(InputAction.CallbackContext context);
        void OnAttack(InputAction.CallbackContext context);
    }
}
