﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public Sound[] sounds;
    public bool disabled; // if set to true all methods do nothing
    public bool playtheme = true;
    [SerializeField]
    private AudioMixer audioMixer;

    public string theme {get; private set;}

    void Awake()
    {
        // ensure game has only one instance of audio manager (for jumping between scenes)
        if (instance is null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        // --

        // populate audio manager with audio source components for every sound
        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.audioClip;
            sound.source.volume = sound.volume;
            sound.source.loop = sound.loop;
            sound.source.pitch = sound.pitch;
            sound.source.outputAudioMixerGroup = sound.group;
        }
        float masterVolume = PlayerPrefs.GetFloat("MasterVolume");

        //Set the music volume to the saved volume
        AdjustMasterVolume(masterVolume);
    }


    public void AdjustMasterVolume(float volume)
    {
        //Update AudioMixer
        audioMixer.SetFloat("MasterVolume", volume);
        //Update PlayerPrefs "Music"
        PlayerPrefs.SetFloat("MasterVolume", volume);
        //Save changes
        PlayerPrefs.Save();
    }

    public void PlayRandomTheme()
    {
        theme = "Theme" + new System.Random().Next(3);
        if (disabled || !playtheme) return;
        Play(theme);
    }

    /// <summary>
    /// Play sound by name
    /// </summary>
    /// <param name="name"> name of sound</param>
    /// <param name="pitch"> play with custom pitch</param>
    public void Play(string name, float pitch = -1)
    {
        if (disabled || name is null) return;
        AudioSource source = Array.Find(sounds, sound => sound.name == name).source;
        if (pitch < 0)
            source.Play();
        else
        {
            float pitchMem = source.pitch;
            source.pitch = pitch;
            source.Play();
            source.pitch = pitchMem;
        }
    }

    public void Pause(string name)
    {
        if (disabled || name is null) return;
        Array.Find(sounds, sound => sound.name == name).source.Pause();
    }

    public void UnPause(string name)
    {
        if (disabled || name is null) return;
        Array.Find(sounds, sound => sound.name == name).source.UnPause();
    }

    /// <summary>
    /// Stop sound by name
    /// </summary>
    public void Stop(string name)
    {
        if (disabled || name is null) return;
        Array.Find(sounds, sound => sound.name == name).source.Stop();
    }

    /// <summary>
    /// Stops all sounds coming from audio manager
    /// </summary>
    public void StopAll()
    {
        if (disabled) return;
        foreach (Sound sound in sounds)
            sound.source.Stop();
    }

    /// <summary>
    /// Mutes/unmutes all sounds coming from audio manager
    /// </summary>
    public void MuteUnmuteAll(bool mute = true)
    {
        if (disabled) return;
        foreach (Sound sound in sounds)
            sound.source.mute = mute;
    }

    public void MuteUnmute(string name, bool mute)
    {
        if (disabled) return;
        Array.Find(sounds, sound => sound.name == name).source.mute = mute;
    }

    /// <summary>
    /// Changes volume of sound by name
    /// </summary>
    /// <param name="name"> name of the sound</param>
    /// <param name="volume"> float between 0 and 1</param>
    /// <param name="time"> time in seconds to change volume</param>
    public void ChangeVolume(string name, float volume, float time = 0)
    {
        if (disabled) return;
        StartCoroutine(ChangeVolumeGradually(name, volume, time));
    }

    private IEnumerator ChangeVolumeGradually(string name, float volume, float time = 0)
    {
        // We find audio source of specified sound
        AudioSource source = Array.Find(sounds, sound => sound.name == name).source;
        float changeSpeed = 0;

        // Gradually change sound over specified duration
        while (Math.Abs(source.volume - volume) > 0.01f)
        {
            changeSpeed += Time.deltaTime / time;
            source.volume = Mathf.Lerp(source.volume, volume, changeSpeed);
            yield return null;
        }

        // Ensure sound changed to exact value
        source.volume = volume;
    }

    [Serializable]
    public class Sound
    {
        public string name;
        public bool loop;
        public AudioClip audioClip;
        public AudioMixerGroup group;
        [Range(0f, 1f)] public float volume = 1f;
        [Range(0f, 2f)] public float pitch = 1f;
        [HideInInspector] public AudioSource source;
    }
}