﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreKeeper : MonoBehaviour
{
    public static int scoreCounter;
    public static int reputationCount;
    public static ScoreKeeper instance;

    public TMP_Text scoreText;

    void Start()
    {
        scoreCounter = 0;
        scoreText = GetComponent<TMP_Text>();
        reputationCount = PlayerPrefs.GetInt("reputation", reputationCount);
        UpdateScore();
    }
    void UpdateScore()
    {
        scoreText.text = "Score: " + scoreCounter;
        Reputation.instance.UpdateReputation();
    }

    public void AddPoints(int pointsToAdd)
    {
        scoreCounter += pointsToAdd;
        reputationCount += pointsToAdd;
        PlayerPrefs.SetInt("reputation", reputationCount);
        UpdateScore();
    }
    private void Awake()
    {
        instance = this;
    }

    void OnDestroy()
    {
        PlayerPrefs.SetInt("reputation", reputationCount);
        PlayerPrefs.Save();
    }
}
