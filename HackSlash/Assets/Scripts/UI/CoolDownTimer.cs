﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CoolDownTimer : MonoBehaviour
    {
        public static float cooldown;
        public static float coolDownTimeLeft = 0;
        public Text cooldownText;
        private float fadeStart;
        private float alpha;

        // Update is called once per frame
        void Start()
        {
            cooldownText.color = new Color(255, 255, 255, 0);
            coolDownTimeLeft = 0;
        }

        void Update()
        {
            if (coolDownTimeLeft == cooldown)
            {
                fadeStart = Time.time;
                alpha = 1;
            }

            if (coolDownTimeLeft > 0)
            {
                cooldownText.text = coolDownTimeLeft.ToString("0.00") + "s";
                cooldownText.color = new Color(1, 1, 1, alpha);
                coolDownTimeLeft -= Time.deltaTime;

                if (alpha > 0)
                    alpha = 1.0f - (Time.time - fadeStart) / cooldown;

            }

            if (coolDownTimeLeft < 0)
            {
                coolDownTimeLeft = 0;
                cooldownText.color = Color.clear;
            }

        }

        public static void startTimer(float cd)
        {
            coolDownTimeLeft = cd;
            cooldown = cd;
        }
        public static float getTimer()
        {
            return coolDownTimeLeft;
        }

    }
}
