﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public AudioManager audioManager;
    public Slider masterVolumeSlider;
    public Toggle soundToggleTick;
    public Toggle themeToggleTick;
    public Toggle fullScreenTick;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;
    public void Start()
    {
        float masterVolume = PlayerPrefs.GetFloat("MasterVolume");
        masterVolumeSlider.value = masterVolume;
        soundToggleTick.isOn = PlayerPrefs.GetInt("SoundToggle") == 1 ? false : true;
        themeToggleTick.isOn = PlayerPrefs.GetInt("ThemeToggle") == 1 ? true : false;
        fullScreenTick.isOn = PlayerPrefs.GetInt("Fullscreen") == 1 ? true : false;
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        if (resolutions.Length != 0)
        {

            for (int i = 0; i < resolutions.Length; i++)
            {
                string option = resolutions[i].width + "x" + resolutions[i].height;
                options.Add(option);
                if (resolutions[i].width == Screen.currentResolution.width &&
                    resolutions[i].height == Screen.currentResolution.height)
                    currentResolutionIndex = i;
            }
        } else{
            string option = "Resolution not available";
            options.Add(option);
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    public void SetMasterVolume(float volume)
    {
        audioManager.AdjustMasterVolume(volume);    
    }

    public void ToggleSound(bool soundToggle) 
    {
        soundToggle = !soundToggle;
        PlayerPrefs.SetInt("SoundToggle", soundToggle ? 1 : 0);
        AudioManager.instance.disabled = soundToggle;
        PlayerPrefs.Save();
    }

    public void ToggleTheme(bool themeToggle)
    {
        PlayerPrefs.SetInt("ThemeToggle", themeToggle ? 1 : 0);
        AudioManager.instance.playtheme = themeToggle;
        PlayerPrefs.Save();
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        PlayerPrefs.SetInt("Fullscreen", isFullscreen ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
}
