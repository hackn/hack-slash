﻿using System.Collections;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace UI
{
    public class InGameManager : MonoBehaviour
    {
        public static bool gameisPaused = false;
        public static bool isDead = false;
        public static bool tutorialActive = false;
        public static bool shopActive = false;
        public float deathScreenDelay = 1f;
        private static bool _isFaded = true;
        private static int _levelCount;

        private GameObject _pauseMenu;
        private GameObject _playerUi;
        private GameObject _deathScreen;
        private GameObject _tutorial;
        private GameObject _shop;
        private Animator _animator;
        private PlayerStats _stats;

        private static readonly int DeathAnim = Animator.StringToHash("death");


        private string[] canvasNameArray =
            {"PlayerUI", "DeathScreenCanvas", "PauseMenuCanvas", "TutorialCanvas", "ShopCanvas"};

        public string MenuScene = "Menu";

        private void Awake()
        {
            _tutorial = GameObject.Find(canvasNameArray[3]);
            _playerUi = GameObject.Find(canvasNameArray[0]);
            _deathScreen = GameObject.Find(canvasNameArray[1]);
            _pauseMenu = GameObject.Find(canvasNameArray[2]);
            _shop = GameObject.Find(canvasNameArray[4]);
            _deathScreen.SetActive(false);
            _pauseMenu.SetActive(false);
            _playerUi.SetActive(false);
            _shop.SetActive(false);
            _tutorial.SetActive(false);
        }

        void Start()
        {
            GameObject.FindWithTag("Player");
            _stats = PlayerStats.Instance;
            _animator = GameObject.FindWithTag("Player").GetComponent<Animator>();
            _deathScreen.GetComponent<CanvasGroup>().alpha = 0;
            _levelCount = Scaling.Instance.levelCount;

            if (_levelCount == 1)
            {
                tutorialActive = true;
                TutorialOn();
                Controls.instance.SpawnPrefab("Prefabs/Items/Swords/spoon_sword", 2.5f);
            }
            else
            {
                ShopOn();
                PlayerData data = SaveSystem.LoadData();
                if (data != null)
                    LoadSavedData(data);
            }

            isDead = false;
        }

        void Update()
        {
            if (_stats.health.currHealth < 0.001 && !isDead)
            {
                isDead = true;
                Death();
            }
        }

        public void OnCancel(InputAction.CallbackContext context)
        {
            if (!context.performed || isDead || tutorialActive || shopActive) return;
            if (gameisPaused) Resume();
            else Pause();
        }

        void LoadSavedData(PlayerData data)
        {
            Controls.instance.SpawnPrefab(data.swordPath, 2.5f);
            _stats.health.maxHealth = data.maxHealth;
            _stats.movement.dashSpeed = data.dashUpgrades[0];
            _stats.movement.dashCoolDown = data.dashUpgrades[1];
            _stats.movement.staminaReduction = (int) Mathf.Ceil(data.dashUpgrades[2]);
            _stats.dUpgradeCount = data.upgradeCounts[0];
            _stats.hUpgradeCount = data.upgradeCounts[1];
        }

        public void Resume()
        {
            AudioManager.instance.Play("Click");
            _pauseMenu.SetActive(false);
            _playerUi.SetActive(true);
            Time.timeScale = 1f;
            gameisPaused = false;
            AudioManager.instance.Stop("MenuTheme");
            AudioManager.instance.UnPause(AudioManager.instance.theme);
        }

        public void TutorialOff()
        {
            Time.timeScale = 1f;
            tutorialActive = false;
            _tutorial.SetActive(false);
            ShopOn();
        }

        public void TutorialOn()
        {
            Time.timeScale = 0f;
            tutorialActive = true;
            _tutorial.SetActive(true);
        }

        public void ShopOn()
        {
            shopActive = true;
            Time.timeScale = 0f;
            _shop.SetActive(true);
        }

        public void ShopOff()
        {
            shopActive = false;
            Time.timeScale = 1f;
            _shop.SetActive(false);
            _playerUi.SetActive(true);
        }

        public void Retry()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Scaling.Instance.levelCount = 1;
            AudioManager.instance.StopAll();
            AudioManager.instance.PlayRandomTheme();
        }

        void Pause()
        {
            AudioManager.instance.Play("Click");
            _pauseMenu.SetActive(true);
            _playerUi.SetActive(false);
            Time.timeScale = 0f;
            gameisPaused = true;
            AudioManager.instance.Pause(AudioManager.instance.theme);
            AudioManager.instance.Play("MenuTheme");
        }

        public void LoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(MenuScene);
            AudioManager.instance.StopAll();
        }

        public void Death()
        {
            AudioManager.instance.StopAll();
            AudioManager.instance.Play("PlayerDeath");
            _animator.SetTrigger(DeathAnim);
            isDead = true;
            _deathScreen.SetActive(true);
            Fade(_deathScreen);
            _pauseMenu.SetActive(false);
            _playerUi.SetActive(false);
            _isFaded = true;
            SaveSystem.DeleteData();
        }

        public void QuitGame()
        {
            AudioManager.instance.Play("Click");
            SaveSystem.DeleteData();
            Debug.Log("Quitting...");
            Application.Quit();
        }

        /// <summary>
        /// Ivykdo fade efekta ant CanvasGroup objekto
        /// </summary>
        /// <param name="canv"></param>
        public void Fade(GameObject canv)
        {
            var canvGroup = canv.GetComponent<CanvasGroup>();

            StartCoroutine(DoFade(canvGroup, canvGroup.alpha, _isFaded ? 1 : 0));

            _isFaded = !_isFaded;
        }

        public IEnumerator DoFade(CanvasGroup canvGroup, float start, float end)
        {
            float counter = 0f;

            while (counter < deathScreenDelay)
            {
                counter += Time.deltaTime;
                canvGroup.alpha = Mathf.Lerp(start, end, counter / deathScreenDelay);

                yield return null;
            }

            Time.timeScale = 0f;
        }
    }
}