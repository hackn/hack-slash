﻿using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private GameObject optionsMenu;
    public string optionsMenuName = "OptionsMenu";

    private void Awake()
    {
        optionsMenu = GameObject.Find(optionsMenuName);
    }

    public void Start()
    {
        optionsMenu.SetActive(false);        
    }

    //Metodas patvarkantis zaidimo paleidima editoriuje
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
        AudioManager.instance?.StopAll();
        AudioManager.instance?.PlayRandomTheme();
        InGameManager.gameisPaused = false;
        InGameManager.isDead = false;
    }

    public void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }

}