﻿using Player;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopMenu : MonoBehaviour
{

    public Inventory inventory;
    public GameObject healtPotionButton;
    public GameObject staminaPotionButton;

    public TMP_Text healthUpText;
    public TMP_Text dashUpText;
    public TMP_Text healthPotText;
    public TMP_Text staminaPotText;
    public TMP_Text reputationText;
    public TMP_Text dashAvailable;
    public TMP_Text healtAvailable;

    public TMP_Text potionAvailable;
    public TMP_Text potionAvailable2;
    private Health playerHealth;
    private Movement movement;
    private PlayerStats stats;

    private float dashSpeedMultiplier = 1.2f;
    private float healthMultiplier = 2;
    public int healthUpgradeCost = 5;
    public int dashUpgradeCost = 100;
    public int healthPotionCost = 20;
    public int staminaPotionCost = 10;
    public int availableUpgrades = 3;
    public int reputationAmount;
    private void Start()
    {
        stats = PlayerStats.Instance;
        stats.itemsBought = 0;
        reputationAmount = PlayerPrefs.GetInt("reputation");
        inventory = GameObject.FindWithTag("Player").GetComponent<Inventory>();
        playerHealth = GameObject.FindWithTag("Player").GetComponent<Health>();
        movement = GameObject.FindWithTag("Player").GetComponent<Movement>();
        UpdateValues();

    }

    void UpdateValues()
    {
        reputationText.text = "Reputation:" + reputationAmount.ToString();
        healthUpText.text = "Upgrade \n Cost:" + healthUpgradeCost;
        dashUpText.text = "Upgrade \n Cost:" + dashUpgradeCost;
        healthPotText.text = "Buy \n Cost:" + healthPotionCost;
        staminaPotText.text = "Buy \n Cost:" + staminaPotionCost;
        dashAvailable.text = stats.dUpgradeCount + "/" + availableUpgrades;
        healtAvailable.text = stats.hUpgradeCount + "/" + availableUpgrades;
        potionAvailable.text = stats.itemsBought + "/" + inventory.slots.Length;
        potionAvailable2.text = stats.itemsBought + "/" + inventory.slots.Length;

        PlayerPrefs.SetInt("reputation", reputationAmount);

    }

    public void UpgradeHealth()
    {
        if ((reputationAmount - healthUpgradeCost) < 0 || stats.hUpgradeCount >= availableUpgrades)
        {
            AudioManager.instance.Play("SwordDrop");
            return;
        }
        AudioManager.instance.Play("Pickup");
        playerHealth.maxHealth = Mathf.Ceil(playerHealth.maxHealth * healthMultiplier);
        reputationAmount -= healthUpgradeCost;
        stats.hUpgradeCount++;
        UpdateValues();
    }
    public void UpgradeDash()
    {
        if ((reputationAmount - dashUpgradeCost) < 0 || stats.dUpgradeCount >= availableUpgrades)
        {
            AudioManager.instance.Play("SwordDrop");
            return;
        }
        AudioManager.instance.Play("Pickup");
        movement.dashSpeed = Mathf.Ceil(movement.dashSpeed * dashSpeedMultiplier);
        movement.dashCoolDown -= 0.5f;
        movement.staminaReduction -= 5;
        reputationAmount -= dashUpgradeCost;
        stats.dUpgradeCount++;
        UpdateValues();

    }
    public void BuyHealthPotion()
    {
        if ((inventory.slots.Length <= stats.itemsBought) || ((reputationAmount - healthPotionCost) < 0))
        {
            AudioManager.instance.Play("SwordDrop");
            return;
        }
        AudioManager.instance.Play("Pickup");
        Instantiate(healtPotionButton, inventory.slots[stats.itemsBought].transform);
        stats.itemsBought++;
        reputationAmount -= healthPotionCost;
        UpdateValues();

    }

    public void BuyStaminaPotion()
    {
        if ((inventory.slots.Length <= stats.itemsBought) || ((reputationAmount - staminaPotionCost) < 0))
        {
            AudioManager.instance.Play("SwordDrop");
            return;
        }
        AudioManager.instance.Play("Pickup");
        Instantiate(staminaPotionButton, inventory.slots[stats.itemsBought].transform);
        stats.itemsBought++;
        reputationAmount -= staminaPotionCost;
        UpdateValues();
    }

}
