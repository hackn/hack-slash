﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelKeeper : MonoBehaviour
{
    private int currlevel;
    private int maxlevel;
    public TMP_Text currLevelText;

    void Start()
    {
        maxlevel = PlayerPrefs.GetInt("level");
        currlevel = Scaling.Instance.levelCount;
        UpdateLevel();
    }

    void UpdateLevel()
    {
        currLevelText.text = "Current cave: " + currlevel;
        if (currlevel > maxlevel)
        {
            maxlevel = currlevel;
        }
    }

    private void OnDestroy()
    {
        PlayerPrefs.SetInt("level", maxlevel);
        PlayerPrefs.Save();
    }
}
