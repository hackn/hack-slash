﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Reputation : MonoBehaviour
{
    public TMP_Text reputationPoints;
    public static Reputation instance;
    public void UpdateReputation() => reputationPoints.text = "Reputation earned: " + PlayerPrefs.GetInt("reputation").ToString();
    private void Start() => UpdateReputation();
    private void Awake()
    {
        instance = this;
    }

    


}
