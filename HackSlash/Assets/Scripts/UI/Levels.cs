﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Levels : MonoBehaviour
{
    public TMP_Text level;
    public static Levels instance;
    public void UpdateLevel() => level.text = "Deepest cave reached: " + PlayerPrefs.GetInt("level").ToString();
    private void Start() => UpdateLevel();
    private void Awake()
    {
        instance = this;
    }
}
