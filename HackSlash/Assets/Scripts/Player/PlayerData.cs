﻿using Gun;
using Player;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public string swordPath;
    public float maxHealth;
    public float[] dashUpgrades;
    public int[] upgradeCounts;
    public PlayerData(PlayerStats stats, Sword sword)
    {
        stats = PlayerStats.Instance;
        maxHealth = stats.health.maxHealth;
        dashUpgrades = new float[3];
        upgradeCounts = new int[2];
        dashUpgrades[0] = stats.movement.dashSpeed;
        dashUpgrades[1] = stats.movement.dashCoolDown;
        dashUpgrades[2] = stats.movement.staminaReduction;
        upgradeCounts[0] = stats.dUpgradeCount;
        upgradeCounts[1] = stats.hUpgradeCount;
        swordPath = sword.prefabPath;
    }
}

