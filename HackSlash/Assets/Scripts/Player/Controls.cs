﻿using System;
using Gun;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Player
{
    /// <summary>
    /// Class for handling player controls.
    /// </summary>
    public class Controls : MonoBehaviour
    {
        public static Controls instance;

        public GameObject gunSlot;
        public GameObject hand;
        public Animator animator;
        public Stamina stamina;
        private static readonly int ComboAttack = Animator.StringToHash("SpecialAttack");
        private static readonly int Fire = Animator.StringToHash("Attack");

        private void Start() => instance = this;

        public void QuickUse(GameObject slot)
        {
            if (slot.transform.childCount != 0)
                slot.transform.GetComponentInChildren<Button>().onClick.Invoke();
        }

        public void DropWeapon(InputAction.CallbackContext context)
        {
            // must have at least 1 weapon
            if (!context.performed || hand.transform.childCount == 0) return;

            var sword = hand.transform.GetChild(0).gameObject;
            var icon = gunSlot.transform.GetChild(0).gameObject;

            SpawnPrefab(sword.GetComponentInChildren<Sword>().prefabPath, 2.5f);

            Destroy(sword);
            Destroy(icon);
            AudioManager.instance.Play("SwordDrop");
        }

        public void SpawnPrefab(string prefabPath, float yOffset)
        {
            var prefab = Resources.Load(prefabPath);
            SpawnPrefab(prefab, yOffset);
        }

        public void SpawnPrefab(Object prefab, float yOffset)
        {
            var position = transform.position + transform.forward * 2;
            position.y += yOffset;
            Instantiate(prefab, position, Quaternion.identity);
        }

        /// <summary>
        ///     Cycles through weapons
        /// </summary>
        public void SelectNextWeapon(InputAction.CallbackContext context)
        {
            if (!context.performed) return;

            SelectNext(hand.transform); // select next weapon icon to display
            SelectNext(gunSlot.transform); // select next gun to place in hand (sets others as inactive)

            AudioManager.instance.Play("NextWeapon");

            // Disables last child and moves it to first position. Enables next last one.
            void SelectNext(Transform transform)
            {
                var childCount = transform.childCount;
                if (childCount == 0) return;

                var slot = transform.GetChild(childCount - 1);
                slot.gameObject.SetActive(false);
                slot.SetAsFirstSibling();
                transform.GetChild(childCount - 1).gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Launches player attack
        /// </summary>
        public void Attack(InputAction.CallbackContext context)
        {
            // don't attack if clicking on UI elements
            if (EventSystem.current.IsPointerOverGameObject()) return;

            if (context.interaction is SlowTapInteraction) // Special attack
            {
                SpecialAttack(context);
            }
            else if (context.performed) // Normal attack performed
            {
                Sword.ready = true;
                AudioManager.instance.Play("SwordSwoosh");
                animator.SetTrigger(Fire);
            }
        }

        /// <summary>
        /// Handles logic when long press of attack button detected 
        /// </summary>
        private void SpecialAttack(InputAction.CallbackContext context)
        {
            // Check if enough stamina
            if (stamina.CurrentStamina < 40) return;

            if (context.started) // Special attack almost ready
            {
                AudioManager.instance.Play("Charge");
            }
            else if (context.performed) // Special attack performed
            {
                Sword.ready = true;
                AudioManager.instance.Play("SwordSwoosh");
                animator.SetTrigger(ComboAttack);
                stamina.CurrentStamina -= 40;
            }
        }

        public void SetSwordReady() => Sword.ready = true;
    }
}