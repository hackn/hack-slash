﻿namespace Player
{
    /// <summary>
    /// Script component for players health
    /// </summary>
    public class PlayerHealth : Health
    {
        protected override string hitSound { get; } = "PlayerTakeDamage";
        protected override string deathSound { get; } = "PlayerDeath";
        public override void Death() => currHealth = 0;

        // #future can be used later to increase vulnerability or left as is
        public override void ScaleHealthByLevel(int seed) { /* Code here */ }

    }
}