﻿using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerStats : MonoBehaviour
{

    public static PlayerStats Instance;
    public PlayerHealth health;
    public Movement movement;
    public int hUpgradeCount = 0;
    public int dUpgradeCount = 0;
    public int itemsBought = 0;
    public GameObject hand;

    void Awake()
    {
        Instance = this;
    }

}
