﻿using System.Collections;
using UnityEngine;

namespace Player
{
    /// <summary>
    /// Player stamina
    /// </summary>
    public class Stamina : MonoBehaviour
    {
        public HealthBar staminaBar;
        public float maxStamina;
        public float regenerationAmount;
        public float regenerationInterval;

        /// <summary>
        /// Usage Example:
        /// CurrentStamina += 12 to increase
        /// CurrentStamina -= 34 to decrease
        /// easy :)
        ///  
        /// Includes lower and upper bound check
        /// Sets StaminaBar value automatically
        /// Increases or decreases gradually
        /// </summary>
        public float CurrentStamina
        {
            get => _currentStamina;
            
            set
            {
                if (value < 0) value = 0;
                if (value > maxStamina) value = maxStamina;

                IEnumerator SetValueGradually()
                {
                    float time = 0f;
                    while (time < 1)
                    {
                        time += Time.deltaTime;
                        _currentStamina = Mathf.Lerp(_currentStamina, value, time);
                        staminaBar.SetValue(_currentStamina);
                        yield return null;
                    }
                }

                StartCoroutine(SetValueGradually());
            }
        }

        private float _currentStamina;

        public void Start()
        {
            CurrentStamina = maxStamina;
            
            // Let stamina go 0 -> maxStamina and begin regeneration later
            Invoke(nameof(BeginRegeneration), 5f);
        }

        /// <summary>
        /// Once started regenerates stamina during intervals
        /// </summary>
        private void BeginRegeneration()
        {
            IEnumerator Increase()
            {
                while (true) // Regenerate forever
                {
                    CurrentStamina += regenerationAmount;
                    yield return new WaitForSeconds(regenerationInterval); // Sleep between regenerations
                }
            }

            StartCoroutine(Increase());
        }
        
    }
}