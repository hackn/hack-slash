﻿using UnityEngine;

namespace Player
{
    public class PlayerAnimation : MonoBehaviour
    {
        private Animator _animator;
        private static int[] _attacksArray;
        private static readonly int Condition = Animator.StringToHash("condition");
        private static int _attackVal;

        void Start()
        {
            _attacksArray = new []
            {
                Animator.StringToHash("attack"),
                Animator.StringToHash("attack2"),
                Animator.StringToHash("attack3")
            };
            _animator = GetComponent<Animator>();
        }
    }
}