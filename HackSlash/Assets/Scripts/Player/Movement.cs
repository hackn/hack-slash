﻿using System.Collections;
using UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class Movement : MonoBehaviour
    {
        public float movementSpeed = 20;
        public float turnSpeed = 10;
        public float dashSpeed = 10;
        public float dashDuration = 0.3f;
        public float dashCoolDown = 2;
        public int staminaReduction = 20;
        
        private Vector2 _input;
        private Quaternion _targetRotation;
        private float _angle;
        private float _cdTimeStamp;
        private Rigidbody _rigidbody;
        private BoxCollider _cl;
        private Stamina _stamina;
        private Animator _animator;

        private readonly int _dash = Animator.StringToHash("dash");
        private static readonly int Condition = Animator.StringToHash("condition");

        void Start()
        {
            _stamina = GetComponent<Stamina>();
            _animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            Rotate();

            if (_input.sqrMagnitude < 0.01) return;
            CalculateDirection();
            PlayerMovement();
        }

        public void Dash(InputAction.CallbackContext context)
        {
            if (!context.performed || // event must be started
                _animator.GetInteger(Condition) != 1 || // player must be in walking state
                !(_stamina.CurrentStamina >= staminaReduction) || // there must be enough stamina
                !(CoolDownTimer.getTimer() < 0.001)) // cooldown must be ended
                return;
            
            _stamina.CurrentStamina -= staminaReduction;
            StartCoroutine(nameof(DashMove));
            AudioManager.instance.Play("Dash");
            CoolDownTimer.startTimer(dashCoolDown);
        }


        public void GetInput(InputAction.CallbackContext context)
        {
            _input = context.ReadValue<Vector2>();

            switch (context.phase)
            {
                case InputActionPhase.Performed:
                    _animator.SetInteger(Condition, 1);
                    break;
                case InputActionPhase.Canceled:
                    _animator.SetInteger(Condition, 0);
                    break;
            }
        }

        void CalculateDirection() => _angle = Mathf.Atan2(_input.x, _input.y) * Mathf.Rad2Deg;

        void Rotate()
        {
            _targetRotation = Quaternion.Euler(0, _angle, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, _targetRotation, turnSpeed * Time.deltaTime);
        }

        IEnumerator DashMove()
        { 
            movementSpeed += dashSpeed;
            _animator.SetTrigger(_dash);
            yield return new WaitForSeconds(dashDuration);
            movementSpeed -= dashSpeed;
        }

        void PlayerMovement()
        {
            Vector3 playerMovement = new Vector3(_input.x, 0, _input.y) * (movementSpeed * Time.deltaTime);
            _rigidbody.MovePosition(transform.position + playerMovement);
        }
    }
}