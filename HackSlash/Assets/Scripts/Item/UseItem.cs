﻿using Player;
using UnityEngine;

namespace Item
{
    public class UseItem : MonoBehaviour
    {
        private GameObject player;
        void Start() => player = GameObject.FindWithTag("Player");

        /// <summary>
        /// Restores player's health by amount
        /// </summary>
        public void UseHealthPotion(int healthToRestore)
        {
            player.GetComponent<PlayerHealth>().RestoreHealth(healthToRestore);
            Destroy(gameObject);
            AudioManager.instance.Play("HealthPotionUse");
        }

        /// <summary>
        /// Restores player's health by amount
        /// </summary>
        public void UseStaminaPotion(int staminaToRestore)
        {
            player.GetComponent<Stamina>().CurrentStamina += staminaToRestore;
            Destroy(gameObject);
            AudioManager.instance.Play("HealthPotionUse");
        }
    }
}