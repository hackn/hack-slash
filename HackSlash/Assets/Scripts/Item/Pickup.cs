﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Inventory inventory;
    public GameObject itemButton;

    private void Start() => inventory = GameObject.FindWithTag("Player").GetComponent<Inventory>();

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            for (int i = 0; i < inventory.slots.Length; i++)
                if (inventory.slots[i].transform.childCount == 0)
                {
                    Instantiate(itemButton, inventory.slots[i].transform, false);
                    Destroy(gameObject.transform.parent.gameObject);
                    AudioManager.instance.Play("Pickup");
                    break;
                }
    }
}