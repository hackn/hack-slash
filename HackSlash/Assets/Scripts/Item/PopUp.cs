﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUp : MonoBehaviour
{
    public Image image;
    public void Called ()
    {
        Debug.Log("Used");
        image.enabled = !image.enabled;
    }
}
