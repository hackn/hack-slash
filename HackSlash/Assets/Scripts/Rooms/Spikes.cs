﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    public float timer = 5f;
    public float timerA = 1f;
    private float active;
    public float notActive;

    void Start()
    {
        notActive = timer;
        active = timerA;
        GetComponent<BoxCollider>().enabled = false;
    }

    void Update()
    {
        notActive -= Time.deltaTime;

        if (notActive <= 0 && active >= 0)
        {
            GetComponent<BoxCollider>().enabled = true;
            active -= Time.deltaTime;
        }
        else if (notActive <= 0 && active <= 0)
        {
            GetComponent<BoxCollider>().enabled = false;
            notActive = timer;
            active = timerA;
        }
    }
}
