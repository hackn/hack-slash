﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Gun;

public class Transition : MonoBehaviour
{
    private MeshCollider collider;
    private MeshRenderer mesh;
    public GameObject boss;
    private PlayerStats stats;
    private Sword sword;

    void Start()
    {
        stats = PlayerStats.Instance;
        boss = GameObject.FindGameObjectWithTag("Boss");
        collider = GetComponent<MeshCollider>();
        mesh = GetComponent<MeshRenderer>();
        collider.enabled = false;
        mesh.enabled = false;
    }

    void FixedUpdate()
    {
        if (boss == null)
        {
            collider.enabled = true;
            mesh.enabled = true;
        }
        else
        {
            collider.enabled = false;
            mesh.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Level up
            Scaling.Instance.levelCount += 1;
            //Delete old save data
            SaveSystem.DeleteData();
            // Find the equipped sword
            int childCount = stats.hand.transform.childCount;
            sword = stats.hand.transform.GetChild(childCount - 1).GetComponentInChildren<Sword>();
            // Save new data for next level
            SaveSystem.SaveData(stats, sword);
            // Load next level
            SceneManager.LoadScene(1);
        }
    }
}
