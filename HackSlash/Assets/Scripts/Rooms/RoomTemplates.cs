﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomTemplates : MonoBehaviour
{
    public GameObject[] bottomRooms;
    public GameObject[] topRooms;
    public GameObject[] leftRooms;
    public GameObject[] rightRooms;
    public GameObject inviWall;
    private float roomCount = 7;
    public bool spawnInvi = false;
    public NavMeshSurface surface;

    public List<GameObject> rooms;

    public float waitTime;
    private bool spawnedBoss;
    public GameObject boss;
    public GameObject transition;
    private float destroyTime;

    void Start()
    {
        if (Scaling.Instance.levelCount > 1)
        {
            roomCount = roomCount * Mathf.Log(Scaling.Instance.levelCount);
        }
        destroyTime = waitTime + 1f;
        Destroy(gameObject, destroyTime);
    }

    void Update()
    {
        if (roomCount <= rooms.Count)
        {
            spawnInvi = true;
        }
        if (waitTime <= 0 && spawnedBoss == false)
        {
            Vector3 v = new Vector3(0f, 0.3f, 0f);
            surface.BuildNavMesh();
            for (int i = 0; i < rooms.Count; i++)
            {
                if (i == rooms.Count - 1)
                {
                    Instantiate(boss, rooms[i].transform.position, Quaternion.identity);
                    Instantiate(transition, rooms[i].transform.position + v, Quaternion.identity);
                    spawnedBoss = true;
                    break;
                }
            }
        }
        else
        {
            waitTime -= Time.deltaTime;
        }
    }
}
