﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomSpawner : MonoBehaviour
{
    public NavMeshSurface surface;
    public int openingDir;
    // 1 --> top spawner
    // 2 --> bottom spawner
    // 3 --> right spawner
    // 4 --> left spawner

    private RoomTemplates templates;
    private int rand;
    private bool spawned = false;
    public float waittTime = 4f;

    void Start()
    {
        Destroy(gameObject, waittTime);
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        Invoke("Spawn", 0.2f);
    }

    void Spawn()
    {
        if (!spawned && !templates.spawnInvi)
        {
            if (openingDir == 1)
            {
                rand = UnityEngine.Random.Range(0, templates.bottomRooms.Length);
                Instantiate(templates.bottomRooms[rand], transform.position, templates.bottomRooms[rand].transform.rotation);
            }
            else if (openingDir == 2)
            {
                rand = UnityEngine.Random.Range(0, templates.topRooms.Length);
                Instantiate(templates.topRooms[rand], transform.position, templates.topRooms[rand].transform.rotation);
            }
            else if (openingDir == 3)
            {
                rand = UnityEngine.Random.Range(0, templates.leftRooms.Length);
                Instantiate(templates.leftRooms[rand], transform.position, templates.leftRooms[rand].transform.rotation);
            }
            else if (openingDir == 4)
            {
                rand = UnityEngine.Random.Range(0, templates.rightRooms.Length);
                Instantiate(templates.rightRooms[rand], transform.position, templates.rightRooms[rand].transform.rotation);
            }
            spawned = true;
        }
        else if (templates.spawnInvi)
        {
            Invoke("invi", 0.5f);
        }
    }
    void invi()
    {
        Instantiate(templates.inviWall, transform.position, templates.inviWall.transform.rotation);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SpawnPoint"))
        {
            if (other.GetComponent<RoomSpawner>().spawned == false && spawned == false)
            {
                Invoke("invi", 0.5f);
            }
            spawned = true;
        }
        if (other.gameObject.name == "Floor")
        {
            Destroy(gameObject);
        }
    }
}
