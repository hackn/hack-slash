﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Player;
using Gun;

public static class SaveSystem
{
    public static string path = Application.persistentDataPath + "/playerData.data";

    public static void SaveData(PlayerStats stats, Sword sword)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream stream = new FileStream(path, FileMode.Create);
        PlayerData data = new PlayerData(stats, sword);

        formatter.Serialize(stream, data);

        stream.Close();
    }

    public static PlayerData LoadData()
    {
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;

            stream.Close();
            return data;
        }
        else
        {
            Debug.Log("Save file not found in" + path);
            return null;
        }
    }

    public static void DeleteData()
    {
        if (File.Exists(path))
            File.Delete(path);

        else
        {
            Debug.Log("Save file not found in" + path);
            return;
        }
    }


}
