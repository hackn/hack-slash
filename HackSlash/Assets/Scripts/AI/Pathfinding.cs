﻿using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public class Pathfinding : MonoBehaviour
    {
        public Animator animator;
        public NavMeshAgent navMeshAgent;
        
        public float escapeTime = 5f;
        public float escape;
        public float dist;

        private GameObject _player;
        private bool _found;

        private void Start() => _player = GameObject.FindGameObjectWithTag("Player");

        private void Update()
        {
            escape -= Time.deltaTime;
            if (_found)
            {
                navMeshAgent.destination = _player.transform.position;
            }
            dist = Vector3.Distance(gameObject.transform.position, _player.transform.position);
            if (dist < 15f)
            {
                escape = escapeTime;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                escape = escapeTime;
                _found = true;
                animator.SetBool("isWalking", true);
            }
            else if (escape < 0)
            {
                _found = false;
                animator.SetBool("isWalking", false);
            }
        }
        
    }
}
