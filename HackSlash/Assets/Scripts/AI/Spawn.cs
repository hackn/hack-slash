﻿using UnityEngine;

namespace AI
{
    public class Spawn : MonoBehaviour
    {
        public float count = 1;
        public GameObject[] mobs;

        private void Start()
        {
            if (Scaling.Instance.levelCount > 1)
                count *= Mathf.Log(Scaling.Instance.levelCount);
            
            for (int i = 0; i < count; i++)
                Instantiate(mobs[Random.Range(0, mobs.Length)], transform.position, Quaternion.identity);
            
            Destroy(gameObject);
        }
    }
}
