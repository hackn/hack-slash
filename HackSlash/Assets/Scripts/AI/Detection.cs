﻿using System.Collections;
using System.Collections.Generic;
using AI;
using UnityEngine;
using UnityEngine.AI;

public class Detection : MonoBehaviour
{
    public GameObject mob;
    private bool movingDown;
    private Rigidbody rb;
    private Pathfinding pathfinding;
    private BoxCollider boxCollider;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pathfinding = mob.GetComponent<Pathfinding>();
        boxCollider = GetComponent<BoxCollider>();
    }
    
    void FixedUpdate()
    {
        Vector3 move = new Vector3(0, 0, 0.01f); ;
        if (movingDown)
            move *= -1;

        rb.MovePosition(transform.position + move);

        if (transform.position.z > 10)
            movingDown = false;
        else if (transform.position.z < -10)
            movingDown = true;
    }
}
