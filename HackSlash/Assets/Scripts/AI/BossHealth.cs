﻿using Player;
using UnityEngine;

namespace AI
{
    public class BossHealth : MobHealth
    {
        public override void ScaleHealthByLevel(int seed)
        {
            maxHealth *= 1.5f;
            if (seed > 1)
            {
                maxHealth *= Mathf.Log(seed);
                healthBar.SetMaxValue(currHealth);
            }
            else
            {
                maxHealth /= 2;
                healthBar.SetMaxValue(maxHealth);
            }
        }

        public override void Death()
        {
            base.Death();
            
            GameObject[] swords = Resources.LoadAll<GameObject>("Prefabs/Items/Swords");
            int index = Random.Range(0, swords.Length - 1);
            Controls.instance.SpawnPrefab(swords[index], 2.5f);
        }
    }
}
