﻿using Gun;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// Script component for mob health
    /// </summary>
    public class MobHealth : Health
    {
        private Animator playerAnimator;

        protected override string hitSound { get; } = "MobTakeDamage";
        protected override string deathSound { get; } = "MobDeath";

        public override void Start()
        {
            base.Start();
            playerAnimator = GameObject.FindWithTag("Player").GetComponent<Animator>();
        }

        public override void ScaleHealthByLevel(int seed)
        {
            if (seed > 1)
            {
                maxHealth *= Mathf.Log(seed);
                healthBar.SetMaxValue(currHealth);
            }
            else
            {
                maxHealth /= 2;
                healthBar.SetMaxValue(maxHealth);
            }
        }

        public override void Death()
        {
            base.Death();
            ScoreKeeper.instance.AddPoints(5); 
        }

        /// <summary>
        /// Gets damage when hit with sword
        /// </summary>
        private void OnTriggerStay(Collider other)
        {
            if (
                other.gameObject.CompareTag("Sword") &&
                Sword.ready &&// one hit per animation
                playerAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") // attack animation is playing
            )
            {
                TakeDamage(other.gameObject.GetComponent<Sword>().damage);

                // is changed to true again in Player_Animation.cs just before starting attack animation
                Sword.ready = false;
            }
        }
        
    }
}