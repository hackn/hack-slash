﻿using UnityEngine;

public abstract class Health : MonoBehaviour
{
    private float _maxHealth = 100;
    public float maxHealth
    {
        get { return _maxHealth; }
        set
        {
            _maxHealth = value;
            healthBar.SetMaxValue(_maxHealth);
            currHealth = _maxHealth;
        }
    }
    public HealthBar healthBar;

    [HideInInspector]
    public float currHealth;

    protected abstract string hitSound { get; }
    protected abstract string deathSound { get; }

    public virtual void Start()
    {
        ScaleHealthByLevel(Scaling.Instance.levelCount);
        currHealth = maxHealth;
    }

    /// <summary>
    /// Changes health based on seed
    /// </summary>
    /// <param name="seed">Higher seed = higher health</param>
    public abstract void ScaleHealthByLevel(int seed);

    /// <summary>
    /// Reduces health by ammount (ensures health is between 0 and max health)
    /// </summary>
    public virtual void TakeDamage(float amount)
    {
        currHealth -= amount;
        currHealth = currHealth < 0 ? 0 : currHealth;
        healthBar.SetValue(currHealth);
        AudioManager.instance.Play(hitSound);

        if (currHealth < 0.0001f) Death();
    }
    
    /// <summary>
    /// Restores specified amount of health (ensures health is between 0 and max health)
    /// </summary>
    public virtual void RestoreHealth(float amount)
    {
        currHealth += amount;
        currHealth = currHealth > maxHealth ? maxHealth : currHealth;
        healthBar.SetValue(currHealth);
    }

    /// <summary>
    /// Called when health reaches 0
    /// </summary>
    public virtual void Death()
    {
        AudioManager.instance.Play(deathSound);
        Destroy(gameObject);
    }

}