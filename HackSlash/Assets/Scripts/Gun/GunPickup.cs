﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Gun
{
    public class GunPickup : MonoBehaviour
    {
        public Sprite icon;
        public EventHandler handler;

        void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            
            // find hand
            var hand = other.transform.Find("Armature/hand_left/weapon_hand");
            var handTransform = hand.transform;

            GameObject sword = gameObject;
            var parent = sword.transform.parent;

            // deselect last weapon
            if (hand.transform.childCount > 0)
                hand.GetChild(hand.transform.childCount - 1).gameObject.SetActive(false);

            // Delete round shadow that sword has
            Destroy(parent.GetChild(transform.parent.childCount - 1).gameObject);

            // Enable normal shadow on sword
            sword.GetComponent<MeshRenderer>().shadowCastingMode = ShadowCastingMode.On;

            // put sword into player's hand
            parent.SetParent(hand);
            sword.transform.position = handTransform.position;
            sword.transform.rotation = handTransform.rotation * Quaternion.Euler(-20, 40, 40);

            var gunSlot = GameObject.FindWithTag("GunSlot").transform;

            // delete last weapon icon
            if (gunSlot.transform.childCount > 0)
                gunSlot.GetChild(gunSlot.transform.childCount - 1).gameObject.SetActive(false);

            GameObject swordButton = new GameObject();
            Image image = swordButton.AddComponent<Image>();
            image.sprite = icon;

            Instantiate(swordButton, gunSlot, false);
            AudioManager.instance.Play("SwordPickup", 0.9f);

            // delete components that are no longer required
            Destroy(GetComponent<Animation>());
            Destroy(GetComponent<GunPickup>()); // delete this script
        }
    }
}