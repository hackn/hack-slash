Crypt of the Cursed
===================

"Crypt of the Cursed" is roguelite action game written in C# using Unity game engine.

Watch trailer on youtube: [Crypt of the Cursed](https://www.youtube.com/watch?v=yQYuanHGgnk)

![Crypt of the cursed](https://bitbucket.org/hackn/hack-slash/raw/4e87dded59f576458ff575b030ee369fb9087f29/HackSlash/Assets/Sprites/Icon.png)


## Download

Available on itch.io: https://hack-slash.itch.io/crypt-of-the-cursed

See also: [Version history](https://bitbucket.org/hackn/hack-slash/wiki/Home)

## Controls


- `W` `A` `S` `D`: Move
- `E`: Switch weapon
- `Q`: Drop weapon
- `Enter` or `Left mouse button`: Attack
- `Space`: Dash
- `1` `2` `3` `4`: Inventory quick use
- `Esc`: Open pause menu